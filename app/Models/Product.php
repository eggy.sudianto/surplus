<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Product;

class Product extends Model
{
    use HasFactory;

    protected $fillable = [
        "name",
        "description",
        "enable",
    ];

    public function products()
    {
        return $this->belongsToMany(Product::class, 'category_products');
    }

}
